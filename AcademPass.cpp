﻿// AcademPass.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;
/*Написав таку міні програмку для зручності. Тут реалізував функцію з завдання, її ім'я Funk також зробив обробку розміру масиву
яка кидає помилку коли розмір квадратного масиву менше 3 або більше 40. Також написав інструкцію з завдання, вона знаходиться на 57 рядку.
Для зручності перевірки є заповнення та вивід матриці і результатів обчислення. Відповіді на теорію та БНФ в коментарях після коду програми*/
template<size_t sizeofmas>
double* Funk(const double (&mass)[sizeofmas][sizeofmas]) {
    if (sizeofmas < 3 || sizeofmas > 40) {
        throw exception("Incorrect size of matrix!");
    }
    else
    {
        double* result = new double[sizeofmas];
        for (size_t i = 0; i < sizeofmas; i++) {
            for (size_t j = 0; j < sizeofmas; j++)
            {
                if (i == j) {
                    double max = mass[i][j];
                    for (size_t k = j + 1; k < sizeofmas; k++)
                    {
                        if (mass[j][k] > max) {
                            max = mass[j][k];
                        }
                    }
                    result[i] = max;
                }
            }
        }
        return result;
    }
}

void ResultOut(const double* presult, const int size) {
    cout << "Result is:" << endl;
    cout << "1st element adress is:" << presult << endl;
    for (size_t i = 0; i < size; i++)
    {
        cout << i + 1 << " Row max value: " << presult[i] << endl;
    }
}

int main()
{
    const int size = 10;
    double matrix[size][size];
    for (size_t i = 0; i < size; i++)
    {
        for (size_t j = 0; j < size; j++)
        {
            matrix[i][j] = rand() % 100;
            matrix[i][j] /= 2.1;
        }
    }
    try {
        double* presult = Funk(matrix);
        cout << "Matrix is:" << endl;
        for (size_t i = 0; i < size; i++)
        {
            cout << "| ";
            for (size_t j = 0; j < size; j++)
            {
                cout << matrix[i][j] << " ";
            }
            cout << "|" << endl;
        }
        ResultOut(presult, size);
        delete[] presult;
    }
    catch (exception error) {
        cout << error.what() << endl;
    }
}

/*
* 1.Що з погляду математики являє собою значення масиву? - з погляду математики масив є вектором,
або ж існує ще один варіант що масив - це матриця, що також на мою думку теж є вірним так як по суті матриця це теж вектор,
вектор не може бути множиною з погляду математики бо в множинах усі елементи унікальні.
* 2.Чим небезпечна операція індексації? - масиви мають фіксовану довжину, тому потрібно стежити щоб не звертатися до елементів,
індекс яких виходить за межі масиву бо це може призвести до непередбачуваних наслідків.
* 3. Які арифметичні дії з адресами дають результат одного з цілих типів? - віднімання однотипних адрес, тобто якщо ми маємо два адресних вирази типу Т* то,
* якщо ми віднімемо  одну адресу від другої, результатом віднімання буде ціла кількість одиниць типу Т, які можна вмістити між цими двома адресами. 
* БНФ які визначають множину ланцюжків символів: послідовність з двох або трьох слів, які розділені крапками й містять кожне не менш ніж дві малі лат. літери:
Як я розумію слова складаються тільки з малих латинських літер.
<smallLetter> ::= a | b | ... | z
<divider> ::= .
<word> ::= <smallLetter> <smallLetter> | <smallLetter> <smallLetter> <word>
<result> ::= <word> <divider> <word> | <word> <divider> <word> <divider> <word>
РБНФ:
<smallLetter> ::= a | b | ... | z
<divider> ::= .
<word> ::=  <smallLetter> <smallLetter> {<smallLetter>}
result ::= <word> <divider> <word> [<divider> <word>]
*/ 
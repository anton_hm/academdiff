﻿#include<iostream>
// Алгоритм для швидкого пошуку входження строки у текст з повтором
using namespace std;
void KnutMorissPrat(string text, string obrz);
int main() {
	string test = "baaabaa";
	string str = "aa";
	KnutMorissPrat(test, str);
}

void fill_mas(int* p, int length, string pattern) {
	p[0] = 0;
	int len = 0;
	int i = 1;
	while (i < length)
	{
		if (pattern[i] == pattern[len]) {
			len++;
			p[i] = len;
			i++;
		}
		else
		{
			if (len == 0) {
				p[i] = 0;
				i++;
			}
			else {
				len = p[len - 1];
			}
		}
	}
}
void KnutMorissPrat(string text, string pattern) {
	int m = pattern.length();
	int n = text.length();
	int count = 0;
	try
	{
		if (n < m) {
			throw 1;
		}
		int i = 0;// Символ який вказує на індекс тексту в якому шукаємо
		int j = 0;//Символ який вказує на індекс pattern
		int* p = new int[m];
		for (int i = 0; i < m; i++)
		{
			p[i] = 1;
		}
		fill_mas(p, m, pattern);
		while (i < n) {
			if (text[i] == pattern[j]) {
				i++;
				j++;
				if (j == m) {
					//cout << "FOUND" << endl;//
					count++;
					//break;//
				}
			}
			else {
				if (j > 0) {
					j = p[j - 1];
				}
				else
				{
					i++;
				}
			}
		}
		if (i == n && count == 0) {
			cout << "NOT FOUND" << endl;
		}
		delete[] p;
	}
	catch (int e)
	{
		if (e == 1) {
			cout << "Text is smaller than pattern" << endl;
		}
	}
	cout << "Amount is: " << count << endl;
}